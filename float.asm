; Routines for moving (copying) numbers

; Fetch a number from a RAM location to ARG
; Input: HL pointer to float in RAM
MOVMA:	PUSH	BC		;Save used registers
    	PUSH	DE
    	PUSH	IX
    	PUSH	HL		;Load HL
    	POP	IX		;Into IX
    	LD	DE,#ARG		;In DE ARG regster address
    	LD	BC,$5		;Move 5 bytes
    	LDIR			;k
    	LD	A,(IX+1)	;In A first byte of mantissa
    	BIT	7,A		;Is bit 7 set?
    	JR	NZ,.ELSE
.IF     LD	(IX+5),$255	;Yes, store 0xFF in sign byte
    	JR	.RETURN		;Goto return
.ELSE	LD	(IX+5),$0	;No, store 0 in sign byte
.RETURN	SET	7,A		;Set omitted bit in mantissa
    	LD	(IX+1),A	;Store A in mantissa[1]
    	POP	IX		;Restore used registers
    	POP	DE
    	POP	BC
    	RET			;Return from subroutine
	
; Copy a number currently in ARG, over into FAC
MOVAF:	PUSH	BC		;Save used registers
    	PUSH	DE		
    	PUSH	HL
    	LD	BC,$6		;Move 7 bytes
    	LD	HL,#ARG		;In HL ARG register address
    	LD	DE,#FAC		;In DE FAC address
    	LDIR			;Copy bytes
    	DEC	HL		
    	LDI			
    	POP	HL		;Restore used registers
    	POP	DE
    	POP	BC
    	RET			;Return from subroutine

; Copy a number currently in FAC, over into ARG
MOVFA:	PUSH	BC		;Save used registers
    	PUSH	DE
    	PUSH	HL
    	LD	BC,$7		;Move 7 bytes
    	LD	HL,#FAC		;In DE ARG address
    	LD	DE,#ARG		;In HL FAC register address
    	LDIR			;Copy bytes
    	DEC	DE
    	LDD	
    	LDD			
    	POP	HL		;Restore used registers
    	POP	DE
    	POP	BC
    	RET			;Return from subroutine

; Fetch a number from a RAM location to FAC
; Input: HL pointer to float in RAM
MOVMF:	PUSH	BC		;Save used registers
    	PUSH	DE
    	PUSH	IX
    	PUSH	HL		;Load HL
    	POP	IX		;Into IX
    	LD	DE,#FAC		;In DE FAC register address
    	LD	BC,$5		;Move 5 bytes
    	LDIR			;Copy 5 bytes
    	BIT	7,(IX+1)	;Is negative?
    	JR	NZ,.ELSE
.IF     LD	(IX+6),$255	;Yes, store FF in sign byte
    	JR	.RETURN		;Goto return
.ELSE	LD	(IX+6),$0	;No, store 0 in sign byte
.RETURN	SET	7,A		;Set omitted bit in mantissa
    	POP	IX		;Restore used registers
    	POP	DE
    	POP	BC
    	RET			;Return from subroutine
	
; Store the number currently in FAC, to a RAM location
; Input: DE pointer to destination in RAM
; Output: none
MOVFM:	PUSH	BC		;Save used registers
    	PUSH	HL
    	PUSH	IX
    	LD	HL,#FAC		;In DE FAC register address
    	LD	BC,$5		;Move up to 5 bytes
    	LDI			;Copy first two bytes
    	LDI			;
    	LD	IX,#FAC		;In IX FAC register address
    	LD	A,$0		;In A 0
    	CP	A,(IX+6)	;Sign = 0?
    	PUSH	DE
    	POP	IX		;In IX pointer &float[1]
    	JR	NZ,.ELSE
.IF     RES	7,(IX+1)	;Clear sign bit
    	JR	.RETURN		;Goto return
.ELSE	SET	7,(IX+1)	;Set sign bit
.RETURN	LDIR			;Copy the other bytes
    	POP	IX		;Restore used registers
    	POP	HL
    	POP	BC
    	RET			;Return from subroutine

; Routines for converting between floating point and other formats

; Convert number in FAC to 16-bit signed integer
; Output: int in IAR
FACINT:	PUSH	BC		;Save used registers
    	PUSH	HL
    	PUSH	IX
    	LD	IX,#FAC
    	LD	A,$127		;
    	CP	A,(IX)		;Is the exponent < 127?
    	JR	C,.ELSE1
.IF1	LD	(HL),$0		;Yes, float is lower than 1
    	INC	HL		;Int is 0
    	LD	(HL),$0
    	JR	.ENDIF1		;Goto assign sign
.ELSE1	PUSH	HL		;Float is higher or equal to 1
    	LD	HL,#FAC+1	
    	LD	DE,#ICR	
    	LD	BC,$3		
    	LDIR			;Move 3 bytes from mantissa to ICR
    	LD	A,$142
    	CP	A,(IX)		;If exponent > 142 float to big
    	JP	P,ILLEGAL_QUANTITY_ERROR ;Goto error table
    	LD	HL,(ICR)	;In HL mantissa first 2 MSB
    	INC	HL		;Test if mantissa is to big
    	JP	Z,ILLEGAL_QUANTITY_ERROR ;Goto error table
    	LD	A,(IX)		;Nope, start converstion
    	LD	C,$128		;In C 1.0's exponent
    	SUB	A,C		;Subtract 128 from accumulator
    	LD	C,A		;Swap A and C
    	LD	A,$24		;In A length of mantissa
    	SUB	A,C		;In A number of shifts
.LOOP	SRL	(IX+15)		;Shift byte 2
    	SRL	(IX+14)		;Shift byte 1
    	JR	NC,.L1
    	SET	7,(IX+15)	;Set MSB in byte 2 if carry set
.L1     SRL	(IX+13)		;Shift byte 0
    	JR	NC,.L2		
    	SET	7,(IX+14)	;Set MSB in byte 1 if carry set
.L2     DEC	A		;Decrement A
        JR	NZ,.LOOP	;Loop if A != 0
.ENDIF1	BIT	0,(IX+6)	;Test sign
        JR	Z,.ELSE2
.IF2	JR	.ENDIF2		;Float is positive
.ELSE2	SET	7,(HL)		;Float is negative
.ENDIF2	POP	IX		;Restore used registers
    	POP	HL
    	POP	BC
    	RET			;Return from subroutine

; Convert number expressed as a zero-terminated ASCII string, to
; floating point number in FAC. Expects string-address in 113H-114H.
; Ex 12.345E+10\0
FIN:	
    	RET

; Convert numerical ASCII-string to floating point number in FAC.
STRVAL:	
    	RET

; Convert number in FAC to a zero-terminated ASCII string.
FOUT:	
    	RET

; Convert 16-bit signed integer to floating point number in FAC
INTFAC:	PUSH	BC		;Save used registers
    	PUSH	DE		
    	PUSH	IX		
    	EX	DE,HL		;Swap DE and HL registers
    	LD	HL,#FAC		;In DE FAC address
    	LD	B,$7		;Byte counter
.FCLR	LD	(HL),$0		;Load 0 in memory pointed by DE
    	INC	HL		;Increment pointer
    	DEC	B		;Increment counter
    	JR	NZ,.FCLR	;Clear FAC loop
    	EX	DE,HL		;Swap them again
    	LD	DE,#FAC+2	;DE points to second byte of mantissa
    	LD	BC,$2		;BC byte counter
    	LDIR			;Transfer int from memory to FAC
    	LD	IX,#FAC		;Load in IX FAC address
    	LD	C,$0		;Load 0 in C (store carry flags)
    	LD	E,C		;And in E (exponent)
    	BIT	$7,(IX)		;Check if negative
    	JR	Z,.LOOP		;No, start shifting stuff, sign byte already at 0
    	DEC	(IX+6)		;Yes, set sign byte at FFH
.LOOP	SLA	(IX+3)		;Shift left int's least significant byte
    	JR	NC,.L1		;If carry set
    	SET	$0,C		;Set bit 0 in C
.L1     SLA	(IX+2)		;Shift left int's most significant byte
    	JR	NC,.L2		;If carry set
    	SET	$1,C		;Set bit 1 in C
.L2     SLA	(IX+1)		;Shift left FAC most significant byte
        BIT	$0,C		;Check if first carry is set
        JR	Z,.L3		;No, try the next
        SET	$0,(IX+2)	;Yes, set int's MSByte LSB
.L3     BIT	$1,C		;Check if second carry is set
        JR	Z,.L4		;No, continue
        SET	$0,(IX+1)	;Yes, set FAC's MSByte LSB
.L4     INC	E		;Increment exponent
    	BIT	$7,(IX+1)	;Check if MSB in FAC is set
    	JR	NZ,.LOOP	;Again while mantissa bit 0 is != 1
    	LD	A,$24		;Yes
    	SUB	A,E		;In A exponent offset
    	LD	E,$127		;In E base exponent
    	ADD	A,E		;In A actual exponent
    	LD	(IX),A		;Store A in exponent byte in FAC
    	POP	IX		;Restore used registers
    	POP	DE		
    	POP	BC		
    	RET			;Return from subroutine

; Convert number in FAC to 32-bit signed integer
FACLONG:	
    	RET
	
; Routines for performing calculations

; Performs the ABS function on the number in FAC 
ABS:	PUSH	HL
    	LD	HL,#FAC+6
    	LD	(HL),$0
    	POP	HL
    	RET

ATN	; Performs the ATN function on the number in FAC
COS	; Performs the COS function on the number in FAC
MUL10	; Multiply the number held in FAC by 10
DIV10	; Divide the number held in FAC by 10. Ignores the sign of the number in FAC, the result is always positive
EXP	; Performs the EXP function on the number in FAC
FADD	; Adds the number in FAC with one stored in RAM
FADDT	; Adds the numbers in FAC and ARG
FDIV	; Divides a number stored in RAM by the number in FAC
FDIVT	; Divides the number in ARG by the number in FAC. Ignores the sign of the number in FAC and treats it as positive number
FMULT	; Multiplies a number from RAM and FAC (clobbers ARG)
FPWR	; Raises a number stored ín RAM to the power in FAC
FPWRT	; Raises the number in ARG to the power in FAC
FSUB	; Subtracts the number in FAC from one stored in RAM
FSUBT	; Subtracts the number in FAC from the number in ARG
 
; Performs the INT function on the number in FAC 
INT:	
    	RET
	
LOG	; Performs the LOG function on the number in FAC 

; Switches sign on the number in FAC, if non-zero 
NEGOP:	PUSH	IX		;Save IX register
    	LD	IX,#FAC		;Load in IX FAC address
    	LD	A,$0		;Load 0 in A
    	CP	A,(IX)		;Check if float is 0
    	JR	Z,.RETURN	;Yes, return
    	BIT	$7,(IX+6)	;No, check if sign is -1
    	JR	Z,.ZERO		;No, jump to .ZERO
    	INC	(IX+6)		;Yes, increment sign (result is 0)
    	JR	.RETURN		;Return
.ZERO	DEC	(IX+6)		;Or decrement sign (result is -1)
.RETURN	POP	IX		;Restore IX register
    	RET			;Return from subroutine

POLY	; Evaluates a polynomial for the value given in FAC 
POLY2	; Evaluates a polynomial with odd powers only, for the value given in FAC 
SIN	; Performs the SIN function on the number in FAC 

; Performs the SGN function on the number in FAC, result is stored in accumulator
SGN:	PUSH	HL		;Save HL register
    	LD	A,$0		;Clear accumulator
    	LD	HL,#FAC+6	;Load in HL FAC sign address
    	BIT	$7,(HL)		;Check if bit 7 in sign is set
    	JR	NZ,.ZERO	;No, jump forward
    	DEC	A		;Yes, decrement A (-1)
    	JR	.RETURN		;And then return
.ZERO	INC	A		;Increment A (1)
.RETURN	POP	HL		;Restore HL register
    	RET			;Return from subroutine

SQR:	; Performs the SQR function on the number in FAC
        RET
TAN	; Performs the TAN function on the number in FAC 

; Routines for comparing numbers
FCOMP	; Compares the number in FAC against one stored in RAM. The result of the comparison is stored in A: 
	; Zero (0) indicates the values were equal.
	; One (1) indicates FAC was greater than RAM and negative one (-1 or $FF) indicates FAC was less than RAM.
	; Also sets processor flags (N,Z) depending on whether the number in FAC is zero, positive or negative
SIGN	; Sets processor flags (N,Z) depending on whether the
	; number in FAC is zero, positive or negative
