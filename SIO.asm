	
;SIO Addresses
SIOA_DATA:	.EQ	0b00001100
SIOA_CMD:	.EQ	0b00001110

SIOB_DATA:	.EQ	0b00001101
SIOB_CMD:	.EQ	0b00001111

;SIO B Write Register ROM values, copied in RAM at boot 
WR0B:	.DR	#0b00011000	;Channel reset
    	.DR	#0b00000010	;Select register 2
WR2B:	.DR	#0b00010000	;Interrupt vector
    	.DR	#0b00010100	;Select register 4
WR4B:	.DR	#0b00000100	;1 stop bit
    	.DR	#0b00000011	;Select register 3
WR3B:	.DR	#0b11000001	;8 bits, enable receiver
    	.DR	#0b00000101	;Select register 5
WR5B:	.DR	#0b01100010	;8 bits, request to send
    	.DR	#0b00010001	;Select register 1
WR1B:	.DR	#0b00010110	;Interrupt on all received characters, status affects vector, enable transmitter
    	.DR	#0b00000110	;Select register 6
WR6B:	.DR	#0b00000000	;0, unused
    	.DR	#0b00000111	;Select register 7
WR7B:	.DR	#0b00000000	;0, unused

;Initialize SIOB
SIOB_INIT:			;SIOB initialization subroutine
    	PUSH	BC		;Save used register
    	PUSH	HL		
    	LD	HL,#SIOB_WRR	;In HL SIOB wr byte sequence
    	LD	B,$15		;Byte count
    	LD	C,#SIOB_CMD	;In C SIOB command address
    	OTIR			;Write values in SIOB write registers
    	EI			;Enable interrupts
    	POP	HL		;Restore used registers
    	POP	BC
    	RET			;Return from subroutine

;Enable SIOB to transmit
SIOB_SEND:			;SIOB start transmission subroutine
    	PUSH	BC		;Save register BC
    	LD	(SIOB_TxR),BC	;In SIOB Tx Register start of string to be transmitted
    	LD	(SIOB_TxR+2),A	;And the length of the string
    	LD	C,#SIOB_CMD	;In C SIOB command address
    	LD	A,$5		;Address of write register 5   
    	LD	B,#SIOB_WRR+5	;In B WR5B value...
    	SET	$3,B		;... with Tx enabled
    	OUT	(C),A		;Select write register 5
    	OUT	(C),B		;Write new value in register
    	POP	BC		;Restore register BC
    	RET			;Return from subroutine
	
;Checks if there is data in the receive queue
SIOB_AVAILABLE:
    	PUSH	BC		;Save register BC
    	LD	A,(SIOB_BACK)	;Load in A back offset  
    	LD	B,A		;Move A in B
    	LD	A,(SIOB_FRONT)	;Load in A front offset
    	CP	A,B		;Compare A and B
    	POP	BC		;Restore register BC
    	RET			;Return from subroutine

;Reads a char from the receive queue
SIOB_READ:
    	PUSH	HL		;Save registers
    	PUSH	DE
    	LD	HL,#SIOB_RxQ	;Load in HL SIOB Rx queue address
    	LD	A,(SIOB_FRONT)	;Load in A the front offset
    	LD	E,A		;Move A to E
    	LD	D,$0		;Load 0 in D
    	ADD	HL,DE		;In HL queue front address
    	LD	D,(HL)		;Load in D the content of queue front
    	INC	A		;Increment A
    	LD	(SIOB_FRONT),A	;Store new front offset
    	LD	A,D		;Move D to A
    	POP	DE		;Restore used registers
    	POP	HL
    	RET			;REturn from subroutine

;Interrupt Serivice Routines

TxCB_ISR:
    	EX	AF,AF'		;Use alternate registers
    	EXX
    	LD	HL,(SIOB_TxR)	;In HL string address 
    	LD	A,(SIOB_TxR+2)	;In A string length
    	LD	C,#SIOB_DATA	;In C SIOB data address
    	LD	B,(HL)		;In B next char to be transmitted
    	OUT	(C),B		;Send char
    	DEC	A		;End of string?
    	JR	NZ,.RETURN	;No, return
    	LD	B,#WR5B		;Yes, disable transmitter, in B WR5B value
    	LD	C,#SIOB_CMD	;In C SIOB command address
    	LD	A,$5		;In A wr5 address
    	OUT	(C),A		;Select WR5...
    	OUT	(C),B		;... then write to it
.RETURN	LD	(SIOB_TxR),HL	;Increase Tx Register value
    	EXX			;Switch back registers
    	EX	AF,AF'		
    	RETI			;Return from subroutine
	
RxCB_ISR:
    	EX	AF,AF'		;Use alternate register set
    	EXX			
    	LD	HL,#SIOB_RxQ	;Load in HL SIOB Rx queue address
    	LD	A,(SIOB_BACK)	;Load in A the back offset
    	LD	E,A		;Move A to E
    	LD	D,$0		;Load 0 in D
    	ADD	HL,DE		;In HL queue back address
    	LD	C,#SIOB_DATA	;Load in C the receive data address
    	IN	B,(C)		;Load in B the received character
    	LD	(HL),B		;Store B in the queue
    	INC	A		;Increment back offset
    	LD	(SIOB_BACK),A	;Store new back offset
    	EXX			;Switch back registers
    	EX	AF,AF'
    	RETI			;Return from subroutine
