; A couple of useful macros

;Push registers onto stack
RPUSH	.MA	reg_list
	
	PUSH	]1
	.XM	]#=1	
	PUSH	]2
	.XM	]#=2
	PUSH	]3
	.XM	]#=3
	PUSH	]4
	.XM	]#=4
	PUSH	]5
	.XM	]#=5
	PUSH	]6
	.XM	]#=6	
	PUSH	]7
	
	.EM

;Pop register from stack
RPOP	.MA	reg_list
	
	POP	]1
	.XM	]#=1	
	POP	]2
	.XM	]#=2
	POP	]3
	.XM	]#=3
	POP	]4
	.XM	]#=4
	POP	]5
	.XM	]#=5
	POP	]6
	.XM	]#=6	
	POP	]7
	
	.EM

;Load a 16-bits register into another 16-bits register
;Unfortunally there isn't an instructon for that (EX exchanges only AF with AF' and DE with HL)
LD	.MA	regs
	.DO ]2=SP
	LD  HL,$0
	ADD HL,SP
	.EL
	PUSH	]1
	POP	]2
	.FI
	.EM

;Exchanges the content of two registers
EX	.MA	regs
	
	PUSH	]1
	PUSH	]2
	POP	]1
	POP	]2
	
	.EM
	
; Creates an array in stack
ARRAY   .MA length

    >LD HL,SP
    PUSH    DE
    LD  DE,]1
    ADD HL,DE
    POP DE
    LD  SP,HL
    
    .EM
	
; Macros for movinf FAC and ARG to and from stack
PUSHF	.MA	addr	;Push FAC/ARG backwards
	
	LD	DE,]1	;Load in DE the register address
	LD	HL,$0	;Cancerous way to
	ADD	HL,SP	;load SP into HL
	EX	DE,HL	;Exchange DE and HL registers
	LD	BC,$6	;Byte Counter = 6
	LDDR		;Move 6 bytes to stack
	LD	B,$6
.LOOP	DEC	SP	;Decrease SP 6 times
	DJNZ	.LOOP
	
	.EM
	
POPF	.MA	addr	;Pop FAC/ARG from stack
	
	LD	DE,]1	;Load in DE the register address
	LD	HL,$0	;I hate
	ADD	HL,SP	;this
	LD	BC,$6	;Byte Counter = 6
	LDIR		;Move 6 bytes from stack
	LD	B,$6
.LOOP	INC	SP	;Increase SP 6 times
	DJNZ	.LOOP
	
	.EM
