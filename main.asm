    	.CR	Z80
    	.TF	SasOS.hex,INT
    	
    	.IN	Macros
    	.IN 	KRAM	
	
    	.SM	CODE		;CODE Segment

ORG:	.OR	0x0000
    	JP	RESET

;SIO local jump table
    	.NO	0x0010
    	JR	TxCB	;Transmit buffer empty
    	JR	ORG
    	JR	RxCB	;Receive character available
	
;Global jump table
TxCB:	JP	TxCB_ISR
RxCB:	JP	RxCB_ISR

    	.IN	SIO
    	.IN	PIO
    	.IN	float
    	.IN	ErrorTable

INIT:	LD	A,$0		
    	LD	I,A		;Interrupt vector at zero page
    	LD	SP,#RAMEND	;Stack Pointer points to RAM end
    	LD	HL,#WR0B	;Load in HL WR0B address
    	LD	DE,#SIOB_WRR	;
    	LD	BC,$30		;
    	LDIR			;
    	LD	HL,#FREE_LIST
    	LD	(HL),$0		;Initialize FreeList pointer (linked list start for dinamic memory)
    	RET

RESET:	CALL	INIT		;Bootloader(?) yeah whatever
    	CALL	SIOB_INIT	;Initialize SIOB
    	LD	BC,#HEH		;HEH
    	CALL	SIOB_SEND	;Tansmit string
    	HALT

HEH:	.AZ	/HELLO, WORLD!/