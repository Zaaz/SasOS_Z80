	.CO

 Kernel RAM map (8kB, doesn't really start at address 0x0000, it's just easier for me to visualize it)
 
 0000	[--------------|--------------|----------------------------------]
 0040	[----------------------------------------------------------------]
 0080	[----------------------------------------------------------------]
 00C0	[----------------------------------------------------------------]
 0100	[----------------------------------------------------------------]
 0140	[----------------------------------------------------------------]
 0180	[----------------------------------------------------------------]
 01C0	[----------------------------------------------------------------]
 0200	[----------------------------------------------------------------]
 0240	[----------------------------------------------------------------] 
 0280	[----------------------------------------------------------------]
 02C0	[----------------------------------------------------------------]
 0300	[----------------------------------------------------------------]
 0340	[----------------------------------------------------------------] 
 0380	[----------------------------------------------------------------]
 03C0	[----------------------------------------------------------------]
 0400	[----------------------------------------------------------------]
 0440	[----------------------------------------------------------------] 
 0480	[----------------------------------------------------------------]
 04C0	[----------------------------------------------------------------]
 0500	[----------------------------------------------------------------]
 0540	[----------------------------------------------------------------] 
 0580	[----------------------------------------------------------------]
 05C0	[----------------------------------------------------------------]
 0600	[----------------------------------------------------------------]
 0640	[----------------------------------------------------------------] 
 0680	[----------------------------------------------------------------]
 06C0	[----------------------------------------------------------------]
 0700	[----------------------------------------------------------------]
 0740	[----------------------------------------------------------------] 
 0780	[----------------------------------------------------------------]
 07C0	[----------------------------------------------------------------]
 0800	[----------------------------------------------------------------]
 0840	[----------------------------------------------------------------] 
 0880	[----------------------------------------------------------------]
 08C0	[----------------------------------------------------------------]
 0900	[----------------------------------------------------------------]
 0940	[----------------------------------------------------------------] 
 0980	[----------------------------------------------------------------]
 09C0	[----------------------------------------------------------------]
 0A00	[----------------------------------------------------------------]
 0A40	[----------------------------------------------------------------] 
 0A80	[----------------------------------------------------------------]
 0AC0	[----------------------------------------------------------------]
 0B00	[----------------------------------------------------------------]
 0B40	[----------------------------------------------------------------] 
 0B80	[----------------------------------------------------------------]
 0BC0	[----------------------------------------------------------------]
 0C00	[----------------------------------------------------------------]
 0C40	[----------------------------------------------------------------] 
 0C80	[----------------------------------------------------------------]
 0CC0	[----------------------------------------------------------------]
 0D00	[----------------------------------------------------------------]
 0D40	[----------------------------------------------------------------] 
 0D80	[----------------------------------------------------------------]
 0DC0	[----------------------------------------------------------------]
 0E00	[----------------------------------------------------------------]
 0E40	[----------------------------------------------------------------] 
 0E80	[----------------------------------------------------------------]
 0EC0	[----------------------------------------------------------------]
 0F00	[----------------------------------------------------------------]
 0F40	[----------------------------------------------------------------] 
 0F80	[----------------------------------------------------------------]
 0FC0	[----------------------------------------------------------------]
 1000	[----------------------------------------------------------------]
 1040	[----------------------------------------------------------------] 
 1080	[----------------------------------------------------------------]
 10C0	[----------------------------------------------------------------]
 1100	[----------------------------------------------------------------]
 1140	[----------------------------------------------------------------] 
 1180	[----------------------------------------------------------------]
 11C0	[----------------------------------------------------------------]
 1200	[----------------------------------------------------------------]
 1240	[----------------------------------------------------------------] 
 1280	[----------------------------------------------------------------]
 12C0	[----------------------------------------------------------------]
 1300	[----------------------------------------------------------------]
 1340	[----------------------------------------------------------------] 
 1380	[----------------------------------------------------------------]
 13C0	[----------------------------------------------------------------]
 1400	[----------------------------------------------------------------]
 1440	[----------------------------------------------------------------] 
 1480	[----------------------------------------------------------------]
 14C0	[----------------------------------------------------------------]
 1500	[----------------------------------------------------------------]
 1540	[----------------------------------------------------------------] 
 1580	[----------------------------------------------------------------]
 15C0	[----------------------------------------------------------------]
 1600	[----------------------------------------------------------------]
 1640	[----------------------------------------------------------------] 
 1680	[----------------------------------------------------------------]
 16C0	[----------------------------------------------------------------]
 1700	[----------------------------------------------------------------]
 1740	[----------------------------------------------------------------] 
 1780	[----------------------------------------------------------------]
 17C0	[----------------------------------------------------------------]
 1800	[----------------------------------------------------------------]
 1840	[----------------------------------------------------------------] 
 1880	[----------------------------------------------------------------]
 18C0	[----------------------------------------------------------------]
 1900	[----------------------------------------------------------------]
 1940	[----------------------------------------------------------------] 
 1980	[----------------------------------------------------------------]
 19C0	[----------------------------------------------------------------]
 1A00	[----------------------------------------------------------------]
 1A40	[----------------------------------------------------------------] 
 1A80	[----------------------------------------------------------------]
 1AC0	[----------------------------------------------------------------]
 1B00	[----------------------------------------------------------------]
 1B40	[----------------------------------------------------------------] 
 1B80	[----------------------------------------------------------------]
 1BC0	[----------------------------------------------------------------]
 1C00	[----------------------------------------------------------------]
 1C40	[----------------------------------------------------------------] 
 1C80	[----------------------------------------------------------------]
 1CC0	[----------------------------------------------------------------]
 1D00	[----------------------------------------------------------------]
 1D40	[----------------------------------------------------------------] 
 1D80	[----------------------------------------------------------------]
 1DC0	[----------------------------------------------------------------]
 1E00	[----------------------------------------------------------------]
 1E40	[----------------------------------------------------------------] 
 1E80	[----------------------------------------------------------------]
 1EC0	[----------------------------------------------------------------]
 1F00	[----------------------------------------------------------------]
 1F40	[----------------------------------------------------------------] 
 1F80	[----------------------------------------------------------------]
 1FC0	[----------------------------------------------------------------]

 I will work on that, I promise
	.EC

RAMSTART:	.EQ	0x0000	;Again, not the actual ram start
RAMEND:		.EQ	0x1FFF	
	
		.SM	RAM		; Define RAM registers ecc
		.OR	RAMSTART	; RAM Start
		
size_t		.EQ	2

;Peripherals

SIOA_WRR:	.BS	15	; SIOA Write registers
SIOB_WRR:	.BS	15	; SIOB Write registers
		.BS	4

;Z80SIO	

RxQueue_size	.EQ	256
TxRegister_size	.EQ	3

SIOB_RxQ:	.BS	RxQueue_size	; Receiver queue
SIOB_FRONT:	.BS	1		; RxQueue front
SIOB_BACK:	.BS	1		; RxQueue back
SIOB_TxR:	.BS	TxRegister_size	; Address of data to be sent and it's length in bytes

;FLOAT

FAC_size	.EQ	7
ARG_size	.EQ	6
QCR_size	.EQ	2
ICR_size	.EQ	2
SAR_size	.EQ	3

FAC:		.BS	FAC_size	; Floating point ACcumulator
ARG:		.BS	ARG_size	; floating point ARGument
QCR:		.BS	QCR_size	; Qint Convertion Register High
ICR:		.BS	ICR_size	; Integer Conversion Register / Qint Conversion Register Low
SAR:		.BS	SAR_size	; String Address Register
STF:		.BS	4		; STandatd Float
TEMP:		.BS	5		; Stores temporary values for float calculations

;DINAMIC MEMORY

free_list_size		.EQ	4

FREE_LIST:		.BS	size_t		; Pointer to the first element of the heap linked list
BRKVAL:			.BS	size_t		; Points to first unallocated part of RAM		
MALLOC_HEAP_START: 	.BS	size_t		; 
MALLOC_HEAP_END:	.BS	size_t		; 
MALLOC_MARGIN:		.EQ	32		; Distance from the top of the stack

	.CO
  
 ***** SIO register values *****

100-101-102-103-104-105-106-107-108-109-10A-10B-10C-10D-10E
10F-110-111-112-113-114-115-116-117-118-119-11A-11B-11C-11D
<-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <-> <->
 |   |   |   |   |   |   |   |   |   |   |   |   |   |   +--> Write register A/B
 |   |   |   |   |   |   |   |   |   |   |   |   |   +------> Wrr A/B address and commands
 |   |   |   |   |   |   |   |   |   |   |   |   +----------> Write register A/B
 |   |   |   |   |   |   |   |   |   |   |   +--------------> Wrr A/B address and commands
 |   |   |   |   |   |   |   |   |   |   +------------------> Write register A/B
 |   |   |   |   |   |   |   |   |   +----------------------> Wrr A/B address and commands
 |   |   |   |   |   |   |   |   +--------------------------> Write register A/B
 |   |   |   |   |   |   |   +------------------------------> Wrr A/B address and commands
 |   |   |   |   |   |   +----------------------------------> Write register A/B
 |   |   |   |   |   +--------------------------------------> Wrr A/B address and commands
 |   |   |   |   +------------------------------------------> Write register A/B
 |   |   |   +----------------------------------------------> Wrr A/B address and commands
 |   |   +--------------------------------------------------> Write register A/B
 |   +------------------------------------------------------> Wrr A/B address and commands
 +----------------------------------------------------------> Write register A/B
 
 ***** PIO register values *****
 
 maybe later
 
 ***** DMA *****
 
 Ahahahaha you wish                                                                                                 actually I wish *sad*

 ***** SIO Tx register and Rx queue *****
 
Rx circular queue
+---------------------------+-------+---------------------------------+-------+------------------------------------------------+
|        free space         | front | >-----------------------------> | back  |                   free space                   |
+---------------------------+-------+---------------------------------+-------+------------------------------------------------+
+-------+---------------------------------+-------+-----------------------------------+-------+--------------------------------+
|»--------------------------------------> | back  |            free space             | front | >-----------------------------»|
+-------+---------------------------------+-------+-----------------------------------+-------+--------------------------------+
<----------------------------------------------------------256-bytes----------------------------------------------------------->
  - Received characters will be inserted in the back of the queue
  - Read characters will be removed from the front
  - the queue is empty when front == back, so if you receive 256 characters the queue will appear empty, but the data can still be read
  
Front and Back

 11E front
 11F back

Tx address register
  - Addresses 120H-121H hold a pointer to data in RAM
  - Address 122H holds the length of the fata to be transmitted (0 disables the transmitter)

120-121-122
<--+--> <->
   |     +--------------------> data length
   +--------------------------> data address

 ***** FLOAT *****                                                                                                                kill me
 
01010011 0 1100001 01110011 01001111 01010011     ~1.00120E-13
<---+--> + <---------------+---------------->
    |    |                 +--> 31-bits Mantissa
    |    +--------------------> Sign bit
    +-------------------------> 8-bit Exponent

 Special values (same as IEEE-754)
Zero
  - 00000000 0 0000000 00000000 00000000 00000000 (positive zero)
  - 00000000 1 0000000 00000000 00000000 00000000 (negative zero)
    tecnically the same thing, but the sign can be carried over in the calculations
  
Infinity
  - 11111111 x 0000000 00000000 00000000 00000000 
    sign can be 1 or 0 for negative or positive infinities

NaN
  - 11111111 x <--------------any--------------->

 Floating Point Registers

FAC (Floating Point Accumulator):
  - Address 123H is the exponent byte
  - Addresses 124H-127H hold the four-byte (32 bit) mantissa
  - Address 128H stores the sign in it's most significant bit; 0H for positive, FFH (-1) for negative.
  - Address 129H contains rounding bits for intermediate calculations.
  
 123-124-125-126-127-128-129
 <-> <------+------> <-> <->
  |         |         |   +----> rounding bits
  |         |         +--------> sign (0, positive/FF, negative)
  |         +------------------> mantissa (32 bits)
  +----------------------------> exponent (8 bits)
 
ARG (Floating Point ARGument it's arranged in the same way as FAC, only seven bytes further up):
  - Address 12AH holds the exponent byte
  - Addresses 12BH-12EH hold the four-byte mantissa
  - Address 12FH holds the sign in it's most significant bit; 0 for positive, FF (-1) for negative.
  
 12A-12B-12C-12D-12E-12F
 <-> <------+------> <->
  |         |         +--------> sign (0 if positive, FF if negative)
  |         +------------------> mantissa (32 bits)
  +----------------------------> exponent (8 bits)
  
ICR and QCR (Integer Conversion Register, QINT Conversion Register):
  - Addresses 130H-133H store the 16-bit result of a float to int conversion
  
 130-131-132-133
 <------------->-----------------> qint (32 bits) 
 <--+--> <--+-->
    |       +--------------------> int (16 bits)
    +----------------------------> tempoary byte
 
SAR (String Address Register):
  - Adresses 133H-134H hold a pointer to a string
  - Address 136H holds NULL(for NULL terminated strings) or string length(for the other strings)
  
 134-135-136
 <--+--> <->
    |     +--------------------> NULL / strlen
    +--------------------------> String address

STF (STandard Float):
  - stores the IEEE-754 standard rappresentation of the float stored in FAC
  
 137-138-139-13A

TEMP (stores temporary values):
  - bytes usage will vary, common uses might be storing a normalized float for an operation
  
 13B-13C-13D-13E-13F
 
 ***** HEAP *****
 
 On it, probably a linked list
 Insert -> O(n), has to search an unallocated spot inside the heap memory
 Remove -> O(n), has to find the memory section to be deallocated
 Since it returns the pointer to that specific node the access is O(1)
 
 every node will contain the pointer to the next node and the size of the allocated memory
 the first node will be stored immediately after the BASIC program
 it will be initialixed to NULL, so if it points to NULL the second node will be placed at the start of the heap memory
 
                           +--------→------------→-----------→--------+
                           ↑                                          ↓
+--------+--------+--------+--------+->--------->-+->------>-+--------+--------+--------+--------+->--------->-+->---------->
|      size       |  next node ptr  | unallocated |   data   |      size       |  next node ptr  | unallocated | increasing 
|     2-bytes     |     2-bytes     |    space    |  nbytes  |     2-bytes     |     2-bytes     |    space    | addresses  
+--------+--------+--------+--------+->--------->-+->------>-+--------+--------+--------+--------+->--------->-+->---------->
<-----------------+----------------->                        <-----------------+----------------->
                  |                                                            |
                  +-------------------> free list "struct" <-------------------+
          
 this limits the maximum memory to ne allocated with one node to 327687 bytes, half of the memory visible to the Z80,
 I think it's more than enough
 
void*
malloc(size_t length):
  save used registers in the stack
  loop through the list to find a chunk of memory the size you need or bigger
    if found, write down its size and pointer to next chunk
    else use the smallest big enough chunk
      if the length is not enough for 2 chunks keep it 
      else split the memory into 2 chunks
    return address
  if there wasn't space inside the free list add it to the end of the list
    if there isn't enough space (end of heap / stack collision) return NULL
    else allocate chunk, return address
  return NULL

void*
calloc(size_t length, byte size):
  call malloc
  if address = NULL, return NULL
  clear memory
  return address

void
free(void * ptr):
  deallocate memory
  if next or previous nodes are free, merge them
  else return
  
void*
realloc(void * ptr, length):
  if ptr = NULL return malloc(length)
  if length < length(ptr) separate memory into 2 chunks, the first allocated and the other free, return ptr
  loop through the free list to find a big enough chunk
    if found allocate it
      if the free space is larger
        split it in two
      copy data to new array
      return address
  if not then we are at the top of the heap
  simpli change the size of the allocated memory
  return address
    
  
  
  sounds like a plan
   
 ***** BASIC *****
 
 Sintax similar to Commodore BASIC V2 (I want a more powerful def function though)

 Variables

+--------+--------+--------+-----------+--------+---------+--------+--------+--------+->--------->-+--------+--------+--------+
|      name       |  type  |  length   | val(0) |  . . .  | val(n) |  next var ptr   | other stuff |  NULL  |  next var ptr   |
|     2 bytes     | 1 byte | 0-3 bytes |        |         |        |     2 bytes     |             | 1 byte |     2 bytes     |
+--------+--------+--------+-----------+--------+---------+--------+--------+--------+->--------->-+--------+--------+--------+
 
Name is 2 bytes long to keep the search algorithm (linear search) simple and fast (and BASIC confusing, yes I'm that kind of evil)
If the name is longer the extra characters gets ignored, if it's shorter the missing character will be NULL
Type can be float, int, const string, pointer
Length bytes are present only if the variable is an array, they can be any number between 1 and 256 (the total number of 
variables stored is 16MB, way more than the Z80 can address at one time)
Values can occupy 5 bytes (float), 2 bytes (int and pointers) or variable (const string is actually an array, an array of const 
string is a matrix) each string must be NULL terminated ascii saved in heap
If a pointer points to 0 will mean that the program run out of memory, an error will be issued and the program will be ended prematurely
and the variable map will be cleared, the code will be left untouched
There will be a memory location to store the current frame address, since I can't afford to lose an index register

 type byte
 7-6-5-4-3-2-1-0
 | | | | | | | +-> type b0  // defines the type of the variable
 | | | | | | +---> type b1
 | | | | | +-----> type b2
 | | | | | 
 | | | | +-------> array (1 = array, 0 = normal variable)
 | | | | 
 | | | +---------> dim b0   // defines the numbers of dimentions of the array
 | | +-----------> dim b1
 | |
 | +-------------> pointer (1 = pointer, 0 = normal variable)
 |
 +---------------> global (1 = global variable, 0 = non global variable)
 
 +---------+---------+---------+----------------+   +---------+---------+------------+
 | type b2 | type b1 | type b0 |      type      |   | dim  b0 | dim  b1 | dimentions |
 +---------+---------+---------+----------------+   +---------+---------+------------+
 |    0    |    0    |    0    | reserved       |   |    0    |    0    |  reserved  |
 |    0    |    0    |    1    | string         |   |    0    |    1    |     1      |
 |    0    |    1    |    0    | int (16-bits)  |   |    1    |    0    |     2      |
 |    0    |    1    |    1    | reserved       |   |    1    |    1    |     3      |
 |    1    |    0    |    0    | long (32-bits) |   +---------+---------+------------+
 |    1    |    0    |    1    | float (40-bits)|   
 |    1    |    1    |    0    | reserved       |   
 |    1    |    1    |    1    | reserved       |   
 +---------+---------+---------+----------------+   
 
Search algorithm: linear search (lazy I know)
  O(n), which is a decent temporal complexity
  starts at the bottom (addresswise) of the map and goes up
  at the bottom there are the newest variables, the ones the user is more likely to use in the current scope (faster search(?))
  if it reaches address 0x0000 then variable not found, insert it at the bottom of the map
  
  the address of the next variable is farely easy to find
  current_address  + 4 + length * (pointer? 2: type & 0x07)	(current_address stored in IX, I guess)
 
Insert -> O(n), the insertion itself is O(1) but before inserting the variable must be searched (linear search O(n))
  Implemented stack frame sistem, the register IY will be the frame pointer, points to the point where the new scope was created
  when the program enters a new scope (a for loop for instance), a new frame is created.
  Variables will be inserted sequentially, if a variable that doesn't exist is used, said variable will be created 
  at the bottom of the map with an initial value of 0.

Remove -> O(n), has o loop through the list until it hits a frame
  this way most of the garbage is automatically collected

advantages:
  - fast insert
  - local variables automatically removed
  - variable name overloading
  - recently created variables are at the top of the map
  - 
  
disadvantages:
  - if a variable name is overloaded the older one can't be accessed until the younger one gets deleted
  - for single variables it occupies a fare amount of memory (45%(float) or 67%(int and pointers) is metadata)
  - 
  
problems:
  - remove one or more frames is slow
  - 
  
 **************************
 ***** WARNING CANCER *****
 **************************
 
 Don't read this paragraph if you have heart condition or easy rage, it might kill you and/or everything around you
 
Using numbers:
  Any type of number will be converted into float before the operation, which means that int and long are far slower than floats
  This has a couple of advantages thought
  First it's easier to code, I only have to do the float part and then anything will be converted into float
  It helps me with error handling, for example, immagine to be adding 2 int together, I would have to I should throw an exception 
  if a carry or an overflow are present, and the oveflow is a pain to compute, while converting the ints to floats and adding them 
  won't throw any exception, and the conversion errors are already handled by the convertion subroutine without the ned for carry 
  and overflow stuff
  Of course this will make int and long operation much slower than float operation, which are quite slow by themselves, 
  but it's BASIC, noone cares how fast it is, also this is the same thing that calculators do, so... 
  In a future version I will do things properly, I promise
 
 Interpreter
 
 Example of BASIC code line
 
 100 FOR I=0 to 9: PRINT "SES:", I: NEXT I: REM prints SES: and the value of I
 
 100 is the line number
 FOR is the first command to be executed
 : is the end of command character, necessary only when there are more than one commands in one line
 PRINT and NEXT are two other commands
 REM is a remark, like a comment
 
<--------------------------------------- SOURCE CODE NODE -------------------------------------->
+--------+--------+->--------------------->----------->--------------------->-+--------+--------+
|  opt code addr  |                           code                            | next line addr  |
|     2-bytes     |                         80-bytes                          |     2-bytes     |
+--------+--------+->--------------------->----------->--------------------->-+--------+--------+

<---------------------------------------- OPT CODE NODE ---------------------------------------->
+--------+--------+->--------------------->----------->--------------------->-+--------+--------+
| instuction addr |                         arguments                         | next line addr  |
|     2-bytes     |                          n-bytes                          |     2-bytes     |
+--------+--------+->--------------------->----------->--------------------->-+--------+--------+
 
 The list of linked lists is growing
 nice
 
 Anyway, a BASIC code line will always occupy 84 bytes, the first 2 will be the address of the optimized version if that line, 
 the next 80 will contain the code and the remaining 2 the addresses of the next line.
 A form of optimization could be the use of a pre parsed expression (se the expression evaluation section), or the address of the
 subroutine for a particular command. If a line starts with a letter it'll be executed right away, else it'll be
 inserted in the linked list through insertion sort the last element of the list will point to NULL indicating the end of the list, as 
 usual
 
 All the structures necessary to execute the code will be placed after the code itself and then freed whe the code ends
 basically it should have a couple of stacks for expression evaluation and the heap start struct, plus a couple of more things 
 that I could add in the near future
 
 When the command RUN is called the list will be executed in order
 
 the command NEW will delete both the BASIC code and the variable map, while CLR will only clear the latter
 
 making progress, are we?
 
 to recognize the commands I'm not sure right now
 I was thinking at an adjacence matrix, but that turned out to be quite troublesome
 screw that, compile stuff
 
 The BASIC Virtual Machine will have a loop with a giant switch-case statement in the middle, which recognises and runs the command
 the error handler will handle with errors generated during compilation, if a piece of code gets correctly compiled than it can't generate errors
 
 ** WARNING: for switch-case I mean a chain of if-elseif-else statements **
 
 ready: (start of BASIC interpreter)
     print ready
     take input characters
     run direct code
     insert indirect code in the code linked list
      - if new line, insert it in the list
      - else modify the line, free che memory occupied by the opt code (if it exists) and set to 0 its pointer
        (so it doesn't have to delete and recompile the entire code every time)
 loop:
     fetch line of code
     there is the optimizes version? //yes -> first 2 bytes != 0
      - yes, jump to that
      - no, enter switch statement
     switch(code.cmd){
         case 1: ... break;
         case 2: ... break;
         .
         .
         .
         default: goto syntax_error
     } (ENDSW)
     end of code? //yes -> last 2 bytes == 0
         - yes, goto ready
         - no, goto loop
     errors:    // error handler
         .      
         syntax_error
         .
     goto ready
     
 case1:  .
         .
         .
         JP     ENDSW
   
 casen:  .              ; Setup compiler
         .
         .
 cmpn:   .              ; Generate compiled code (throws errors)
         .
         .
 lkn     .              ; Link it
         .
         .
 runn    .              ; And run it
         .
         .
         JP     ENDSW
         
 the first 2 bytes of the code contains the address of 'runn', since it can't be compiled again it's executed right away.
 
 +-------+------------------+-------+        +-------+------------------+-------+            +-------+------------------+------+
 |  OPT  |       CODE       | NEXT  +---+--->|  OPT  |       CODE       | NEXT  +-----+----->|  OPT  |       CODE       |  0   |
 +-------+------------------+-------+   ↑    +-------+------------------+-------+     ↑      +-------+------------------+------+
     ↓                                  |        ↓                                    |          ↓
 +----------+---+------+                |    +----------+---+----------+---+------+   |      +---------+---+------+
 | OPT CODE |';'| NEXT +----------------+    | OPT CODE |':'| OPT CODE |';'| NEXT +---+      | OP CODE |';'|  0   |
 +----------+---+------+                     +----------+---+----------+---+------+          +---------+---+------+
 <--------+-------->                         <-----------------+------------------>
          ↓                                                    ↓
 One command per line                              Multiple commands per line
 
 typedef struct{
    char code[80];          // actual code
    opt_code_line * faster; // points to its optimized version
    code_line * next;       // points to next node
 }code_line;
 
 typedef struct{
    command * ptr;          // points to the implementation of the command
    argument arg_list;      // an array of arguments
    code_line * next;       // points to next node
 }opt_code_line;
 
 the argument list has a length defined during compilation and contains addresses for variables / evaluation trees and 
 the variable infos if it has to be created

                   <------------------------- at most 256 bytes -------------------------->
 +--------+--------+--------+--------+--------+-------+--------+--------+--------+--------+--------+--------+
 |     cmd ptr     |      addr0      | type0  | . . . |      addrn      | typen  | endchr |  next line ptr  |
 |     2 bytes     |     2 bytes     | 1 byte |       |     2 bytes     | 1 byte | 1 byte |     2 bytes     |
 +--------+--------+--------+--------+--------+-------+--------+--------+--------+--------+--------+--------+
 
 the 256 bytes limit for the arguments is due to the length of the pool I'll use to create the argument list, 
 also 256 bytes seems a pretty long argument list to me
 
 Mmmmm I guess I need some control bits
 
 7-6-5-4-3-2-1-0
 | | | | | | | +-> scope b0 // keeps track of the current scope (0 = global)
 | | | | | | +---> scope b1
 | | | | | +-----> scope b2
 | | | | +-------> scope b3
 | | | +---------> scope b4
 | | | 
 | | +-----------> reserved
 | | 
 | +-------------> direct   // identifies a command running in direct mode
 | 
 +---------------> inline   // identifies a command in the same line as others, like 40 PRINT COS(X): GOTO 20, 'GOTO' is an inline command
 
 This bits will affect the flow of the program

 About the optimization stuff
 It's a half way between compilation and interpretation, sort of compile while running or just in time compilation
 When the interpreter starts analizing the code it enters a giant switch case, where the cases are the BASIC commands
 When a match is fount it jumps to a piece of code which "compiles" the code line into something easyer to handle, then executes it.
 It would be a waste to throw away the "compiled" version of that piece of code, so it saves it in a linked list which (if present) will 
 replace the slower verbose line code.
 
 Example, enters this code PRINT "SES:", SES+(5*TAN(X))/2
 
 first searches the command PRINT in the switch case
 then converts the PRINT into its address (where the execution sbr starts), then converts the variables into their addresses (where you 
 can find them in memory) and expressions into evaluation trees, these informaions are then saved into a list node connected to the 
 parent node of the original instruction
 When this line code will be reached it won't enter the swich case, but will automatically jump to the subroutine to run it
 
 So, the first time it encounters a particular line of code it will take a bit of time to execute, but all the other times it should be 
 noticebly faster, at the cost of almost double memory usage.
 
 In case there are more than one instruction per line code it will create more list nodes in series.
 
 problem, how do I free all of them when they becomes obsolete?

 Expresson evaluation
 
 I was thinking (not really, that's an idea from a friend of mine)
 There are a couple of major ways to evaluate an expression, one which uses 2 stacks and one which uses one stack and an evaluation tree
 Using the stacks is relatively easy, non recursive, gives the result right away, but it's not the definition of fast. Imagine you have
 an expression inside a loop, it must be evaluated every iteration. Quite slow right?
 How about the binary tree? Well The downside is that the expression must first converted from infix into postfix (reverse polish) 
 notation, that requires a stack(I can use the system stack ,I guess), and then has to be inserted in the tree itself. Seems slower 
 right? But who sed that you have to do this for each iteration? Once converted into a tree it can be saved somewere end deleted when it 
 isn't of use anymore, meaning that it would be relatively slow only the first time it's used, which is quite nice.
 
 typedef struct{
    variable * address;     // address of variable
    function * ptr;         // address of function (NULL = raw value, else one of the implemented functions such as SIN() COS() ABS() ecc)
    eval_tree_node * left;  // left branch
    eval_tree_node * right; // right branch
 }eval_tree_node;
 
	.EC
	